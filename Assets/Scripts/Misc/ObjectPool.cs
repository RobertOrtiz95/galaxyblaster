﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField]
    private BulletBase bullet = null;

    private List<BulletBase> pool = new List<BulletBase>();
    [SerializeField]
    private int poolCount = 15;

    private void Start()
    {
        CreatePool();
    }

    private void CreatePool()
    {
        for(int i = 0; i < poolCount; i++)
        {
            GameObject go = Instantiate(bullet.gameObject, transform);
            go.SetActive(false);
            pool.Add(go.GetComponent<BulletBase>());
        }
    }

    public BulletBase CallPlayerBullet(Vector3 pos, Quaternion rot)
    {
        for(int i = 0; i < pool.Count; i++)
        {
            if (!pool[i].gameObject.activeInHierarchy)
            {
                pool[i].transform.position = pos;
                pool[i].transform.rotation = rot;
                pool[i].gameObject.SetActive(true);
                return pool[i];
            }
        }

        return null;
    }
}
