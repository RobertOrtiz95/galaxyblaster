﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBase : MonoBehaviour
{
    [SerializeField]
    protected int bulletDamage = 10;
    [SerializeField]
    protected Rigidbody bulletRb = null;
    protected virtual void OnTriggerObject(Collider other)
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        OnTriggerObject(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Boundary")
            gameObject.SetActive(false);
    }
}
