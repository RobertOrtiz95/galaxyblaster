﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditBehaviour : MonoBehaviour
{
    private const int CREDIT_AMOUNT = 1;
    private const float SPEED_MIN = 5.0f, SPEED_MAX = 5.0f;

    private const float ROT_SPEED_MIN = .5f, ROT_SPEED_MAX = 5.0f;


    private float creditSpeed = 0.0f;
    [SerializeField]
    private Rigidbody creditRigidbody = null;
    private Vector3 creditMovement = Vector3.zero;
    private Vector3 rotation = Vector3.zero;

    private void OnEnable()
    {
        creditSpeed = Random.Range(SPEED_MIN, SPEED_MAX);
        creditMovement.z = -creditSpeed;
        creditRigidbody.velocity = creditMovement;

        rotation.x = Random.Range(ROT_SPEED_MIN, ROT_SPEED_MAX);
        rotation.y = Random.Range(ROT_SPEED_MIN, ROT_SPEED_MAX);
        rotation.z = Random.Range(ROT_SPEED_MIN, ROT_SPEED_MAX);
    }

    private void FixedUpdate()
    {
        transform.Rotate(rotation);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            gameObject.SetActive(false);
            GameManager.GetGameManager().GetCreditManager().AddCredits(CREDIT_AMOUNT);
        }
    }
}
