﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private Button startButton = null;
    [SerializeField]
    private Button selectShipButton = null;

    private void OnEnable()
    {
        startButton.onClick.AddListener(OnStartButtonPressed);
        selectShipButton.onClick.AddListener(OnSelectShipButtonPressed);
    }

    private void OnDisable()
    {
        startButton.onClick.RemoveListener(OnStartButtonPressed);
        selectShipButton.onClick.RemoveListener(OnSelectShipButtonPressed);
    }

    private void OnStartButtonPressed()
    {
        GameManager.GetGameManager().StartGame();
    }
    
    private void OnSelectShipButtonPressed()
    {
        GameManager.GetGameManager().OpenShipSelectionScreen();
    }
}
