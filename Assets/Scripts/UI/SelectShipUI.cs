﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SelectShipUI : MonoBehaviour
{
    private const string UNLOCKED_TEXT = "Select";
    private const string LOCKED_TEXT = ":Credits";
    private const string REFILL_TEXT = " Credits";
    [Header("Ship Information")]
    [SerializeField]
    private TMP_Text shipNameText = null;
    [SerializeField]
    private Button refillHealthButton = null;
    [SerializeField]
    private TMP_Text refillHealthButtonText = null;
    [SerializeField]
    private Button refillFuelButton = null;
    [SerializeField]
    private TMP_Text refillFuelButtonText = null;
    [SerializeField]
    private Slider healthSlider = null;
    [SerializeField]
    private Slider fuelSlider = null;

    [Header("Ship Selection")]
    [SerializeField]
    private Button selectButton = null;
    [SerializeField]
    private TMP_Text selectButtonText = null;

    [SerializeField]
    private Button nextShipButton = null;
    [SerializeField]
    private Button prevShipButton = null;

    [SerializeField]
    private Button backButton = null;

    private void OnEnable()
    {
        selectButton.onClick.AddListener(OnSelectButtonPressed);
        nextShipButton.onClick.AddListener(OnNextButtonPressed);
        prevShipButton.onClick.AddListener(OnPrevButtonPressed);
        backButton.onClick.AddListener(OnBackButtonPressed);
        refillFuelButton.onClick.AddListener(OnFuelRefillButtonPressed);
        refillHealthButton.onClick.AddListener(OnHealthRefillButtonPressed);
    }

    private void OnDisable()
    {
        selectButton.onClick.RemoveListener(OnSelectButtonPressed);
        nextShipButton.onClick.RemoveListener(OnNextButtonPressed);
        prevShipButton.onClick.RemoveListener(OnPrevButtonPressed);
        backButton.onClick.RemoveListener(OnBackButtonPressed);
        refillFuelButton.onClick.RemoveListener(OnFuelRefillButtonPressed);
        refillHealthButton.onClick.RemoveListener(OnHealthRefillButtonPressed);
    }

    private void OnHealthRefillButtonPressed()
    {
        Ship ship = GameManager.GetGameManager().GetShipManager().GetCurrentShipIndex();
        ship.RefillHealth();
    }

    private void OnFuelRefillButtonPressed()
    {
        Ship ship = GameManager.GetGameManager().GetShipManager().GetCurrentShipIndex();
        ship.RefillHealth();
    }

    private void OnPrevButtonPressed()
    {
        Ship ship = GameManager.GetGameManager().GetShipManager().PrevShip();
        shipNameText.text = ship.shipName;
        healthSlider.maxValue = ship.GetMaxHealth();
        healthSlider.value = ship.GetCurrentHealth();

        fuelSlider.maxValue = ship.GetMaxFuel();
        fuelSlider.value = ship.GetCurrentFuel();
        refillFuelButtonText.text = ship.GetRefillPrice() + REFILL_TEXT;
        refillHealthButtonText.text = ship.GetRefillPrice() + REFILL_TEXT;

        if (ship.GetUnlocked())
            selectButtonText.text = UNLOCKED_TEXT;
        else
            selectButtonText.text = ship.GetShipPrice() + LOCKED_TEXT;
    }

    private void OnNextButtonPressed()
    {
        Ship ship = GameManager.GetGameManager().GetShipManager().NextShip();
        shipNameText.text = ship.shipName;
        healthSlider.maxValue = ship.GetMaxHealth();
        healthSlider.value = ship.GetCurrentHealth();

        fuelSlider.maxValue = ship.GetMaxFuel();
        fuelSlider.value = ship.GetCurrentFuel();

        if (ship.GetUnlocked())
            selectButtonText.text = UNLOCKED_TEXT;
        else
            selectButtonText.text = ship.GetShipPrice() + LOCKED_TEXT;
    }

    private void OnSelectButtonPressed()
    {
        Ship ship = GameManager.GetGameManager().GetShipManager().GetCurrentShipIndex();
        if (ship.GetUnlocked())
            GameManager.GetGameManager().GetShipManager().SetShip();
        else
            ship.UnlockShip();

        if (ship.GetUnlocked())
        {
            selectButtonText.text = UNLOCKED_TEXT;
            GameManager.GetGameManager().GetShipManager().SetShip();
        }
        else
            selectButtonText.text = ship.GetShipPrice() +  LOCKED_TEXT;
    }

    private void OnBackButtonPressed()
    {
        GameManager.GetGameManager().MainMenu();
    }
}
