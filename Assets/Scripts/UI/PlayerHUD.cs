﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerHUD : MonoBehaviour
{
    [SerializeField]
    private TMP_Text scoreText = null;
    [SerializeField]
    private TMP_Text countDownText = null;
    public void SetAnnouncementText(string countDownText) { this.countDownText.text = countDownText; }
    public void SetScore(int score) { scoreText.text = "score : " + score.ToString("0000"); }
    [SerializeField]
    private Slider healthSlider = null;
    [SerializeField]
    private Slider fuelSlider = null;

    public void SetShipInfo(Ship shipInfo)
    {
        fuelSlider.maxValue = shipInfo.GetMaxFuel();
        fuelSlider.value = shipInfo.GetCurrentFuel();

        healthSlider.maxValue = shipInfo.GetMaxHealth();
        healthSlider.value = shipInfo.GetCurrentHealth();
    }
}
