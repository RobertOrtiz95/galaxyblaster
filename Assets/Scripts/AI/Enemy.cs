﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private const float MIN_POS_X = -10.0f, MAX_POS_X = 10.0f;
    public int shipDamage = 5;
    [SerializeField]
    protected int pointWorth = 10;
    private const int MAX_ENEMY_HEALTH = 100;
    [SerializeField]
    protected int enemyHealth = 100;
    [SerializeField]
    protected Rigidbody enemyRigidbody = null;
    [SerializeField]
    protected float downMovementSpeed = 3.0f;
    protected Vector3 movement = Vector3.zero;

    [SerializeField]
    protected EnemyShoot enemyShoot = null;

    private void OnEnable()
    {
        enemyRigidbody.velocity = Vector3.zero;
        enemyHealth = MAX_ENEMY_HEALTH;
    }

    public void Damage(int dmg)
    {
        enemyHealth -= dmg;
        if(enemyHealth <= 0)
        {
            GameManager.GetGameManager().GetScoreManager().AddScore(pointWorth);
            GameManager.GetGameManager().GetCreditManager().CallForCredit(transform.position);
            gameObject.SetActive(false);
        }
    }

    protected virtual void EnemyMovement()
    {
        movement.z = -downMovementSpeed;
        enemyRigidbody.velocity = movement;
    }

    private void FixedUpdate()
    {
        EnemyMovement();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Ship>() != null)
        {
            collision.gameObject.GetComponent<Ship>().Damage(shipDamage);
            gameObject.SetActive(false);
            enemyRigidbody.velocity = Vector3.zero;
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Boundary")
        {
            Debug.Log("Exited boundary box");
            gameObject.SetActive(false);
        }
    }
}
