﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.ShaderGraph.Internal;
using UnityEngine;

public class EnemyA2 : Enemy
{
    private const float IDLE_TIME_MIN = 1.0f, IDLE_TIME_MAX = 3.0f;
    private const float ROT_Y_START = 220;
    private float rotationSpeed = 30.0f;
    private bool isFireArching = false;

    private float idleTimer = 0.0f;
    [SerializeField]
    private float idleTime = 1.0f;

    private float archTime = 3.0f;
    private float archTimer = 0.0f;

    private void OnEnable()
    {
        idleTime = Random.Range(IDLE_TIME_MIN, IDLE_TIME_MAX);
    }
    protected override void EnemyMovement()
    {
        if (isFireArching)
            ArcFire();
        else
            Idle();

        base.EnemyMovement();
    }

    private void Idle()
    {
        idleTimer += Time.deltaTime;
        if(idleTimer > idleTime)
        {
            isFireArching = true;
            idleTimer = 0;
            transform.localRotation = Quaternion.Euler(0, 35, 0);
            enemyShoot.SetFiring(true);
        }
    }
    private void ArcFire()
    {
        archTimer += Time.deltaTime;
        transform.Rotate(0,-rotationSpeed * Time.deltaTime, 0);

        if(archTimer > archTime)
        {
            isFireArching = false;
            idleTime = Random.Range(IDLE_TIME_MIN, IDLE_TIME_MAX);
            enemyShoot.SetFiring(false);
            transform.localRotation = Quaternion.Euler(Vector3.zero);
            archTimer = 0.0f;
        }
    }
}
