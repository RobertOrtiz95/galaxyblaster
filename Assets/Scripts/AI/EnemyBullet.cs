﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : BulletBase
{
    private float bulletSpeed = 10;
    private Vector3 bulletMovement = Vector3.zero;

    private void FixedUpdate()
    {
        transform.position += transform.forward * -bulletSpeed * Time.deltaTime;
    }
    protected override void OnTriggerObject(Collider other)
    {
        if(other.GetComponent<Ship>() != null)
        {
            other.GetComponent<Ship>().Damage(bulletDamage);
            gameObject.SetActive(false);
        }
        base.OnTriggerObject(other);
    }
}
