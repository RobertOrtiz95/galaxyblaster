﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
    public Transform bulletSpawn = null;
    [SerializeField]
    private float timeBetweenShots = 1.0f;
    private float bulletTimer = 0.0f;
    private bool firing = false;
    public void SetFiring(bool firing) { this.firing = firing; }

    private void Update()
    {
        if (firing)
            Fire();
    }

    private void Fire()
    {
        bulletTimer += Time.deltaTime;
        if(bulletTimer > timeBetweenShots)
        {
            GameManager.GetGameManager().GetEnemyBulletPool().CallPlayerBullet(bulletSpawn.position, transform.localRotation);
            bulletTimer = 0;
        }
    }
}
