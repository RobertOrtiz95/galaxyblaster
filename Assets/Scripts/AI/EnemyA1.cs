﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyA1 : Enemy
{
    private const float MIN_BANK_SPEED = -5.0f, MAX_BANK_SPEED = 5.0f;

    private const float MIN_BANK_TIME = 1.0f, MAX_BANK_TIME = 3.0f;
    private const float MIN_IDLE_TIME = .5f, MAX_IDLE_TIME = 1.0f;
    [SerializeField]
    private float idleTime = 1.0f;
    [SerializeField]
    private float bankTime = 1.0f;
    private float idleTimer = 0.0f;
    private float bankTimer = 0.0f;

    private float bankSpeed = 0.0f;
    private Vector3 shipBank = Vector3.zero;

    private bool isBanking = false;

    private void OnEnable()
    {
        bankSpeed= Random.Range(MIN_BANK_SPEED, MAX_BANK_SPEED);
        shipBank.x = bankSpeed;
        enemyRigidbody.velocity = shipBank;
        bankTime = Random.Range(MIN_BANK_TIME, MAX_BANK_TIME);
        idleTime = Random.Range(MIN_IDLE_TIME, MAX_IDLE_TIME);
        isBanking = false;
        enemyShoot.SetFiring(true);
    }

    private void OnDisable()
    {
        idleTimer = 0;
        bankTimer = 0;
        isBanking = false;
        enemyShoot.SetFiring(false);
    }

    protected override void EnemyMovement()
    {

        if (isBanking)
            Bank();
        else
            Idle();
        base.EnemyMovement();
    }

    private void Idle()
    {
        idleTimer += Time.deltaTime;

        if(idleTimer > idleTime)
        {
            isBanking = true;
            bankSpeed= Random.Range(MIN_BANK_SPEED, MAX_BANK_SPEED);
            movement.x = bankSpeed;
            bankTime = Random.Range(MIN_BANK_TIME, MAX_BANK_TIME);
            idleTimer = 0;
        }
    }

    private void Bank()
    {
        bankTimer += Time.deltaTime;
        
        if (bankTimer > bankTime)
        {
            isBanking = false;
            movement.x = 0;
            idleTime = Random.Range(MIN_IDLE_TIME, MAX_IDLE_TIME);
            bankTimer = 0;
        }
    }

}
