﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private const float MIN_POS_X = -10, MAX_POS_X = 10;
    private const float MIN_POS_Z = -14, MAX_POS_Z = 14;

    [SerializeField]
    private Rigidbody playerRb = null;
    [SerializeField]
    private int playerSpeed = 10;
    public void SetPlayerSpeed(int playerSpeed) { this.playerSpeed = playerSpeed; }

    private Vector3 movement = Vector3.zero;
    private float x = 0, z = 0;

    private float tilt = 10;
    private Vector3 tiltRotation = Vector3.zero;
    [SerializeField]
    private bool movementActive = false;
    public void SetPlayerActive(bool movementActive) { this.movementActive = movementActive; }
    public bool GetPlayerActive() { return movementActive; }

    private void OnEnable()
    {
        playerRb.velocity = Vector3.zero;
        movement.x = playerSpeed;
        movement.z = playerSpeed;
    }

    private void FixedUpdate()
    {
        if (!movementActive)
            return;
        MovePlayer();
        ClampPosition();
    }

    private void ClampPosition()
    {
        movement.x = Mathf.Clamp(transform.position.x, MIN_POS_X, MAX_POS_X);
        movement.z = Mathf.Clamp(transform.position.z, MIN_POS_Z, MAX_POS_Z);
        transform.position = movement;
    }

    private void MovePlayer()
    {
        x = Input.GetAxisRaw("Horizontal");
        tiltRotation.z = tilt * x;
        transform.rotation = Quaternion.Euler(tiltRotation);
        
        z = Input.GetAxisRaw("Vertical");

        movement.x = x * playerSpeed;
        movement.z = z * playerSpeed;

        playerRb.velocity = movement;
    }
}
