﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : BulletBase
{
    [SerializeField]
    private float bulletSpeed = 10;
    private Vector3 playerMovement = Vector3.zero;

    private void Start()
    {
        playerMovement.z = bulletSpeed;
        bulletRb.velocity = playerMovement;
    }

    protected override void OnTriggerObject(Collider other)
    {
        if(other.GetComponent<Enemy>() != null)
        {
            other.GetComponent<Enemy>().Damage(bulletDamage);
            gameObject.SetActive(false);
        }
        base.OnTriggerObject(other);
    }
}
