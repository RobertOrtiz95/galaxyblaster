﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public Transform bulletSpawn1 = null;
    public Transform bulletSpawn2 = null;
    [SerializeField]
    private bool shootActive = true;
    public void SetShootActive(bool shootActive) { this.shootActive = shootActive; }

    private void FixedUpdate()
    {
        if (Input.GetButtonDown("Fire1") && shootActive)
        {
            GameManager.GetGameManager().GetPlayerBulletPool().CallPlayerBullet(bulletSpawn1.position, bulletSpawn1.rotation);
            GameManager.GetGameManager().GetPlayerBulletPool().CallPlayerBullet(bulletSpawn2.position, bulletSpawn2.rotation);
        }
    }
}
