﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    [SerializeField]
    private int price = 10;
    public int GetShipPrice() { return price; }
    private const int REFILL_PRICE = 1;
    public int GetRefillPrice() { return REFILL_PRICE; }

    public string shipName = string.Empty;
    [SerializeField]
    private int maxFuel = 10;
    public int GetMaxFuel() { return maxFuel; }
    [SerializeField]
    private int currentFuel = 10;
    public int GetCurrentFuel() { return currentFuel; }
    [SerializeField]
    private int maxHealth = 100;
    public int GetMaxHealth() { return maxHealth; }
    [SerializeField]
    private int currentHealth = 100;
    public int GetCurrentHealth() { return currentHealth; }

    public void RefillHealth()
    {
        if (currentHealth == maxHealth)
            return;
        if (GameManager.GetGameManager().GetCreditManager().CanPurchaseWithCredits(REFILL_PRICE))
            currentHealth = maxHealth;
    }

    public void RefillFuel()
    {
        if (currentFuel == maxFuel)
            return;
        if (GameManager.GetGameManager().GetCreditManager().CanPurchaseWithCredits(REFILL_PRICE))
            currentFuel = maxFuel;
    }

    [Header("required component references")]

    [SerializeField]
    private PlayerShoot playerShoot = null;
    [SerializeField]
    private PlayerMovement playerMovement = null;
    private bool playerActive = false;
    public void SetPlayerActive(bool playerActive)
    {
        this.playerActive = playerActive;
        playerShoot.SetShootActive(this.playerActive);
        playerMovement.SetPlayerActive(this.playerActive);
    }
    [SerializeField]
    private float timeBetweenFuelUse = 1.0f;
    private int fuelUsePerSecond = 1;
    private float fuelTimer = 0.0f;

    [SerializeField]
    private bool unlocked = false;
    public void UnlockShip()
    {
        if (!GameManager.GetGameManager().GetCreditManager().CanPurchaseWithCredits(price))
            return;
        unlocked = true;
        GameManager.GetGameManager().GetShipManager().SetShip();
    }
    public void SetUnlocked(bool unlocked) { this.unlocked = unlocked; }
    public bool GetUnlocked() { return unlocked; }

    private void OnEnable()
    {
        if (GameManager.GetGameManager().GetCurrentGameState() == GameState.GAME_MODE)
            GameManager.GetGameManager().GetUIManager().GetPlayerHUD().SetShipInfo(this);
    }

    private void FixedUpdate()
    {
        if(playerMovement.GetPlayerActive())
            UseFuel();
    }
    private void UseFuel()
    {
        fuelTimer += Time.deltaTime;

        if(fuelTimer > timeBetweenFuelUse)
        {
            currentFuel -= fuelUsePerSecond;
            GameManager.GetGameManager().GetUIManager().GetPlayerHUD().SetShipInfo(this);
            if (currentFuel <= 0)
            {
                currentFuel = 0;
                GameManager.GetGameManager().GameOver();
            }
            fuelTimer = 0;
        }
    }

    public void Damage(int damage)
    {
        currentHealth -= damage;
        GameManager.GetGameManager().GetUIManager().GetPlayerHUD().SetShipInfo(this);
        if(currentHealth <= 0)
        {
            currentHealth = 0;
            GameManager.GetGameManager().GetUIManager().GetPlayerHUD().SetShipInfo(this);
            GameManager.GetGameManager().GameOver();
        }
    }
}
