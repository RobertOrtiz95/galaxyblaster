﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipManager : MonoBehaviour
{
    public Transform spawnLocation = null;
    public Transform shipDisplayLocation = null;

    [SerializeField]
    private Ship[] ships = null;
    public void TurnOffAllShips()
    {
        for (int i = 0; i < ships.Length; i++)
            ships[i].gameObject.SetActive(false);
    }
    private Ship currentShip = null;
    public Ship GetCurrentShip() { return currentShip; }

    private int currentShipIndex = 0;

    private void Start()
    {
        currentShip = ships[currentShipIndex];
    }

    public void SetShip()
    {
        currentShip = ships[currentShipIndex];
    }

    private void ResetShips()
    {
        for (int i = 0; i < ships.Length; i++)
        {
            ships[i].transform.position = spawnLocation.position;
            ships[i].gameObject.SetActive(false);
        }
    }
    public Ship NextShip()
    {
        ResetShips();
        currentShipIndex = NextIndex();
        ships[currentShipIndex].gameObject.SetActive(true);
        ships[currentShipIndex].transform.position = shipDisplayLocation.position;
        return ships[currentShipIndex];
    }

    public Ship PrevShip()
    {
        ResetShips();
        currentShipIndex = PrevIndex();
        ships[currentShipIndex].gameObject.SetActive(true);
        ships[currentShipIndex].transform.position = shipDisplayLocation.position;
        return ships[currentShipIndex];
    }

    public Ship GetCurrentShipIndex()
    {
        return ships[currentShipIndex];
    }

    private int NextIndex()
    {
        currentShipIndex++;
        if (currentShipIndex > ships.Length - 1)
            currentShipIndex = 0;

        return currentShipIndex;
    }

    private int PrevIndex()
    {
        currentShipIndex--;
        if (currentShipIndex < 0)
            currentShipIndex = ships.Length - 1;

        return currentShipIndex;
    }

    public void OpenShipSelection()
    {
        for(int i = 0; i < ships.Length; i++)
        {
            ships[i].SetPlayerActive(false);
            ships[i].transform.position = shipDisplayLocation.position;
            ships[i].transform.rotation = shipDisplayLocation.rotation;
            currentShip.gameObject.SetActive(true);
        }
    }

    public void SpawnShip()
    {
        currentShip.transform.position = spawnLocation.position;
        currentShip.transform.rotation = spawnLocation.rotation;
        currentShip.gameObject.SetActive(true);
    }
}
