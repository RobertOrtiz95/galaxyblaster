﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager instance = null;
    public static GameManager GetGameManager() { return instance; }

    [SerializeField]
    private UIManager uiManager = null;
    public UIManager GetUIManager() { return uiManager; }
    private GameState currentGameState = GameState.MAIN_MENU;
    public GameState GetCurrentGameState() { return currentGameState; }
    [Header("Object pools")]
    [SerializeField]
    private ShipManager shipManager = null;
    public ShipManager GetShipManager() { return shipManager; }
    [SerializeField]
    private CreditManager creditManager = null;
    public CreditManager GetCreditManager() { return creditManager; }

    [SerializeField]
    private ObjectPool playerBulletPool = null;
    public ObjectPool GetPlayerBulletPool() { return playerBulletPool; }
    [SerializeField]
    private ObjectPool enemyBulletPool = null;
    public ObjectPool GetEnemyBulletPool() { return enemyBulletPool; }
    [SerializeField]
    private EnemyManager enemyManager = null;
    [SerializeField]
    private ScoreManager scoreManager = null;
    public ScoreManager GetScoreManager() { return scoreManager; }
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
            Destroy(gameObject);
    }


    public void GameOver()
    {
        enemyManager.TurnOffAll();
        enemyManager.SetSpawning(false);
        shipManager.GetCurrentShip().SetPlayerActive(false);
        uiManager.GetPlayerHUD().SetAnnouncementText("GameOver\nReturning to base");
        StartCoroutine(GameOverCountDown());
    }

    private IEnumerator GameOverCountDown()
    {
        WaitForSeconds wait = new WaitForSeconds(1);
        for(int i = 10; i > 0; i--)
        {
            yield return wait;
        }
        currentGameState = GameState.MAIN_MENU;
        uiManager.GameStateChanged(currentGameState);
        SceneManager.LoadScene((int)currentGameState);
        uiManager.GetPlayerHUD().SetAnnouncementText(string.Empty);
        shipManager.GetCurrentShip().gameObject.SetActive(false);
    }

    public void MainMenu()
    {
        currentGameState = GameState.MAIN_MENU;
        uiManager.GameStateChanged(currentGameState);
        SceneManager.LoadScene((int)currentGameState);
        shipManager.TurnOffAllShips();
    }

    public void OpenShipSelectionScreen()
    {
        currentGameState = GameState.SELECT_SHIP;
        uiManager.GameStateChanged(currentGameState);
        SceneManager.LoadScene((int)currentGameState);
        shipManager.OpenShipSelection();
    }

    public void StartGame()
    {
        if (shipManager.GetCurrentShip().GetCurrentHealth() < 0)
            return;
        currentGameState = GameState.GAME_MODE;
        uiManager.GameStateChanged(currentGameState);
        SceneManager.LoadScene((int)GameState.GAME_MODE);
        shipManager.SpawnShip();
        StartCoroutine(CountDownToStart());
    }

    private IEnumerator CountDownToStart()
    {
        WaitForSeconds wait = new WaitForSeconds(1);
        uiManager.GetPlayerHUD().SetAnnouncementText("3");
        for(int i = 3; i >= 0; i--)
        {
            yield return wait;
            uiManager.GetPlayerHUD().SetAnnouncementText(i.ToString());
        }
        uiManager.GetPlayerHUD().SetAnnouncementText(string.Empty);
        shipManager.GetCurrentShip().SetPlayerActive(true);
        enemyManager.SetSpawning(true);
    }
}
