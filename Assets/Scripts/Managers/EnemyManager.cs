﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    private const float SPAWN_POS_Z = 15;
    private const float SPAWN_POS_X_MIN = -12, SPAWN_POS_X_MAX = 12;
    private Vector3 spawnPos = Vector3.zero;
    public Transform poolParent = null;
    [SerializeField]
    private Enemy enemyA1 = null;
    [SerializeField]
    private Enemy enemyA2 = null;

    private List<Enemy> enemyPool = new List<Enemy>();
    private int poolCount = 15;

    private bool spawning = false;

    private float timer = 0.0f;
    private float timeBetweenSpawns = 3.0f;

    public void SetSpawning(bool spawning) 
    {
        if (!spawning)
            for (int i = 0; i < enemyPool.Count; i++)
                enemyPool[i].gameObject.SetActive(false);
        this.spawning = spawning; 
    }

    private void Start()
    {
        CreatePool();
        spawnPos.z = SPAWN_POS_Z;
    }

    private void FixedUpdate()
    {
        if (spawning)
            Spawn();
    }

    private void Spawn()
    {
        timer += Time.deltaTime;
        if(timer > timeBetweenSpawns)
        {
            spawnPos.x = Random.Range(SPAWN_POS_X_MIN, SPAWN_POS_X_MAX);
            Enemy e = GetNextEnemy();
            e.gameObject.SetActive(true);
            e.transform.position = spawnPos;
            timer = 0;
        }
    }

    public void TurnOffAll()
    {
        for (int i = 0; i < enemyPool.Count; i++)
            enemyPool[i].gameObject.SetActive(false);
    }

    private void CreatePool()
    {
        bool flip = false;
        for(int i = 0; i < poolCount; i++)
        {
            if (flip)
                enemyPool.Add(CreateEnemy(enemyA1));
            else
                enemyPool.Add(CreateEnemy(enemyA2));
            flip = !flip;
        }
    }

    private Enemy GetNextEnemy()
    {
        for (int i = 0; i < enemyPool.Count; i++)
            if (!enemyPool[i].gameObject.activeInHierarchy)
                return enemyPool[i];

        return null;
    }

    private Enemy CreateEnemy(Enemy enemy)
    {
        GameObject go = Instantiate(enemy.gameObject, poolParent);
        go.SetActive(false);
        return go.GetComponent<Enemy>();
    }
}
