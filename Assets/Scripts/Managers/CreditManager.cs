﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditManager : MonoBehaviour
{
    public Transform creditPoolParent = null;
    [SerializeField]
    private int currentCredits = 100;
    public void AddCredits(int creditAmount)
    { 
        currentCredits += creditAmount;
        GameManager.GetGameManager().GetUIManager().SetCreditsText(currentCredits);
    }

    [SerializeField]
    private CreditBehaviour credit = null;
    private List<CreditBehaviour> credits = new List<CreditBehaviour>();

    [SerializeField]
    private int dropChance = 20;
    private const int DROP_CHANCE_MIN = 0, DROP_CHANCE_MAX = 101;

    private void Start()
    {
        CreatePool();
        GameManager.GetGameManager().GetUIManager().SetCreditsText(currentCredits);
    }

    private void CreatePool()
    {
        for(int i = 0; i < 10; i++)
        {
            GameObject go = Instantiate(credit.gameObject, creditPoolParent);
            go.SetActive(false);
            credits.Add(go.GetComponent<CreditBehaviour>());
        }
    }

    public bool CanPurchaseWithCredits(int creditAmount) 
    {
        if (currentCredits < creditAmount)
            return false;
        currentCredits -= creditAmount;
        return true;
    }

    private bool CheckIfChance()
    {
        int rando = Random.Range(DROP_CHANCE_MIN, DROP_CHANCE_MAX);

        if (rando < dropChance)
            return true;
        else
            return false;
    }
    public void CallForCredit(Vector3 position)
    {
        if (CheckIfChance())
            for (int i = 0; i < credits.Count; i++)
                if (!credits[i].gameObject.activeInHierarchy)
                {
                    credits[i].transform.position = position;
                    credits[i].gameObject.SetActive(true);
                    return;
                }

    }
}
