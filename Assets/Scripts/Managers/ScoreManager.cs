﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    private int currentScore = 0;
    public int GetCurrentScore() { return currentScore; }
    public void AddScore(int currentScore) 
    { 
        this.currentScore += currentScore;
        GameManager.GetGameManager().GetUIManager().GetPlayerHUD().SetScore(this.currentScore);
    }

}
