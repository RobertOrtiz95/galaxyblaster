﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private const string CREDITS_TEXT = "credits : ";
    public GameObject[] gameUIs = null;
    [SerializeField]
    private MainMenu mainMenu = null;
    public MainMenu GetMainMenuUI() { return mainMenu; }
    [SerializeField]
    private PlayerHUD playerHUD = null;
    public PlayerHUD GetPlayerHUD() { return playerHUD; }
    [SerializeField]
    private SelectShipUI selectShipUI = null;
    public SelectShipUI GetSelectShipUI() { return selectShipUI; }
    [SerializeField]
    private TMP_Text creditsText = null;
    public void SetCreditsText(int creditsText) { this.creditsText.text = CREDITS_TEXT + creditsText.ToString(); }
    public void GameStateChanged(GameState state)
    {
        for (int i = 0; i < gameUIs.Length; i++)
            gameUIs[i].SetActive(false);
        gameUIs[(int)state].SetActive(true);
    }
}
